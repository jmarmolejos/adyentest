AdyenTest.Components.App = {
    title: {},
    byline: {},

    element: {},
    
    create: function(elementSelector) {
        this.element = $(elementSelector);

        title = this.element.find('#app_title');
        byline = this.element.find('#app_byline');
        
        $('#radius_input').on('input', function (ev) {
            $('#radius_label').text(ev.target.value + " meters");
        });

        var app = this;
        $('#radius_input').on('change', function(ev) {
            app.reloadData();
        });

        $('#radius_label').text($('#radius_input').val() + " meters");

        this.reloadData();
    },

    reloadData: (function() {
        var app = this;
        navigator.geolocation.getCurrentPosition(function(position) {
            var ll = position.coords.latitude + "," + position.coords.longitude;

            var currentRadius = $('#radius_input').val();

            AdyenTest.Services.FourSquareApi.getVenues(ll, currentRadius).then((function (result) {
                this.update(result);
            }).bind(app));
        });
    }),

    update: function(appState) {
        title.text(appState.response.headerFullLocation);
        byline.text(appState.response.totalResults + " venues found around your location.");
        var venueList = AdyenTest.Components.VenueList.create('#venue_list', appState.response.groups[0].items);
    }
};