AdyenTest.Components.VenueList = {
    element: {},
    
    create: function(elementSelector, componentState) {
        this.element = $(elementSelector);
        this.element.empty();

        var venueListItems = componentState.map((function(venue){

            var listItem = $('<li></li>');

            this.element.append(listItem);

            return AdyenTest.Components.Venue.create(listItem, venue);
        }).bind(this));
    }
};