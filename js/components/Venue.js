AdyenTest.Components.Venue = {
    element: {},

    create: function(elementSelector, componentState) {
        if (typeof elementSelector === "string") {
            this.element = $(elementSelector);
        } else {
            this.element = elementSelector;
        }

        var title = this.createDynamicComponent('<h3></h3>', componentState.venue.name);

        var distance = this.createDynamicComponent('<span></span>', componentState.venue.location.distance + " meters from you.");

        var categories = componentState.venue.categories.map(function(item) {
            return item.name;
        });
        var category = this.createDynamicComponent('<span></span>', categories.join(", "));
        
        var priceLevel = this.createDynamicComponent('<span></span>', componentState.venue.price !== undefined ? "Prices: " + componentState.venue.price.message : 'n/a');

        var location = this.createDynamicComponent('<span></span>', componentState.venue.location.formattedAddress.join(", "));
    },

    createDynamicComponent: function(markup, content) {
        var component = $(markup);
        this.element.append(component);
        component.text(content);

        return component;
    }
};