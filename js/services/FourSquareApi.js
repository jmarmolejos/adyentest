AdyenTest.Services.FourSquareApi = {
    getVenues: function(coordinates, radiusInMeters) {
        radiusInMeters = radiusInMeters || 0;
        
        // Having these here is really keeping me awake at night ...
        var clientSecret="OLENFQKFCCNDCFWJU5MQM5AALV4CATPSFMUHHVCIFVG2WJ5W";
        var clientId="BM5BPMTELFE30IRFBUSAMA2BX3JXLZA3TGX1UP3MLYJAGZUW";

        var apiUrl = "https://api.foursquare.com/v2/venues/explore?ll={{COORDINARTES}}&client_id={{CLIENTID}}&client_secret={{CLIENTSECRET}}&radius={{DISTANCE}}&v=20170710&sortByDistance=1";

        apiUrl = apiUrl
                    .replace("{{CLIENTID}}", clientId)
                    .replace("{{CLIENTSECRET}}", clientSecret)
                    .replace("{{DISTANCE}}", radiusInMeters)
                    .replace("{{COORDINARTES}}", coordinates);

        return $.get(apiUrl);
    }
};